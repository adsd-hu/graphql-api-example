const express = require("express");
const expressGraphQL = require("express-graphql").graphqlHTTP;
const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
} = require("graphql");
const app = express();

const books = require("./books.json");
const authors = require("./authors.json");
const genres = require("./genres.json");

const AuthorType = new GraphQLObjectType({
  name: "Author",
  description: "This represents an author",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
    dateOfBirth: { type: GraphQLNonNull(GraphQLString) },
    country: { type: GraphQLNonNull(GraphQLString) },
    books: {
      type: new GraphQLList(BookType),
      resolve: (author) => {
        return books.filter((book) => book.authorId === author.id);
      },
    },
  }),
});

const GenreType = new GraphQLObjectType({
  name: "Genres",
  description: "This represents a genre",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
    books: {
      type: new GraphQLList(BookType),
      resolve: (genre) => {
        return books.filter((books) => books.genreId === genre.id);
      },
    },
  }),
});

const BookType = new GraphQLObjectType({
  name: "Book",
  description: "This represents a book",
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    title: {
      type: new GraphQLNonNull(GraphQLString),
    },
    authorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    author: {
      type: AuthorType,
      resolve: (book) => {
        return authors.find((author) => author.id === book.authorId);
      },
    },
    genreId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    genre: {
      type: GenreType,
      resolve: (book) => {
        return genres.find((genre) => genre.id === book.genreId);
      },
    },
  }),
});

const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    book: {
      type: BookType,
      description: "A single book",
      args: {
        id: { type: GraphQLInt },
        title: { type: GraphQLString },
      },
      resolve: (parent, args) => {
        return books.find(
          (book) => book.id === args.id || book.title === args.title
        );
      },
    },
    books: {
      type: new GraphQLList(BookType),
      description: "List of all Books",
      resolve: () => books,
    },
    author: {
      type: AuthorType,
      description: "A single author",
      args: {
        id: { type: GraphQLInt },
        name: { type: GraphQLString },
      },
      resolve: (parent, args) => {
        return authors.find(
          (author) => author.id === args.id || author.authorId === args.name
        );
      },
    },
    authors: {
      type: new GraphQLList(AuthorType),
      description: "List of all authors",
      resolve: () => authors,
    },
    genre: {
      type: GenreType,
      description: "A single author",
      args: {
        id: { type: GraphQLInt },
        name: { type: GraphQLString },
      },
      resolve: (parent, args) => {
        return genres.find(
          (genre) => genre.id === args.id || genre.name === args.name
        );
      },
    },
    genres: {
      type: new GraphQLList(GenreType),
      description: "List of all genres",
      resolve: () => genres,
    },
  }),
});

const schema = new GraphQLSchema({
  query: RootQueryType,
});

app.listen(3000, () => console.log("server is running!"));

app.use("/graphql", expressGraphQL({ schema: schema, graphiql: true }));
